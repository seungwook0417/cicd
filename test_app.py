import unittest
import app

class Testapp(unittest.TestCase):
    def test_add(self):
        h = app.Hello()
        res = h.add(2,3)
        self.assertEqual(int(res), 5)

    def test_sub(self):    
        h = app.Hello()
        res = h.sub(3,2)
        self.assertEqual(int(res), 1)

if __name__ == "__main__":
    unittest.main()
