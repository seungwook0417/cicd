from flask import Flask, request

app = Flask(__name__)

class Hello:
    def say(self):
        return 'hello bob from 이승욱'
    
    def add(self, x, y):
        return str(x+y)
    
    def sub(self, x, y):
        return str(x-y)

@app.route('/')
def hello_world():
    return 'Hello Bob from 이승욱'

@app.route('/add')
def add():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return h.add(a,b)

@app.route('/sub')
def sub():
    a = (int)(request.args.get('a'))
    b = (int)(request.args.get('b'))
    return h.sub(a,b)

if __name__ == '__main__':
    h = Hello()
    app.run(host='0.0.0.0', port=8120)
